﻿using Microsoft.Azure.WebJobs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using WinSCP;

namespace _3CX_Integration
{
    public class ServerInfo {
        public Protocol Protocol { get; set; } = Protocol.Sftp;
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string SshHostKeyFingerPrint { get; set; }
        public string SshPrivateKeyPath { get; set; }
        public string DatabaseName { get; set; }
    }
    public class LogHelper
    {
        // Set up session options and DB name      
        public static List<ServerInfo> GetServerInfo(ExecutionContext executionContext )
        {
            // Not sure yet how to save the SshPrivateKey yet            
            // Get all servers that need to be checked and load them into this list           
            var listOfServerInfo = new List<ServerInfo>
            {
                new ServerInfo
                {
                    HostName = "techneauxtest.la.3cx.us",
                    UserName = "admin",
                    SshHostKeyFingerPrint = "ssh-ed25519 255 r6AE55v3QOCXkVDhL6HZEfoPAhKNDmYqyH5QcgBXdZw=",
                    SshPrivateKeyPath = $"{Directory.GetParent(executionContext.FunctionDirectory).FullName}\\Files\\TechneauxTest.ppk",
                    DatabaseName = "3cx_Integration"
                }
            };

            return listOfServerInfo;            
        }

        // The location of the WinSCP.exe
        public static string WinSCPFile(ExecutionContext executionContext) => $"{Directory.GetParent(executionContext.FunctionDirectory).FullName}\\Files\\WinSCP.exe";

        // Returns the connection string specifically for the server we're working on
        public static string ConnectionString(string dbName) => $"Server=localhost\\SQLEXPRESS;Database={dbName};Integrated Security=true;Trusted_Connection=yes;MultipleActiveResultSets=true";

        // These two are query strings to save data to database. If fields are added/removed, these need to be updated to reflect.
        public static string RawQuery => @"Insert into dbo.RawLogData (Name, String) output inserted.RawLogId values (@Name, @String);";
        public static string ParseQuery => @"Insert into dbo.ParsedLogData (RawLogId, HistoryId,CallId,Duration,TimeStart,TimeAnswered,TimeEnd,ReasonTerminated,FromNo,ToNo,FromDn,ToDn,DialNo,ReasonChanged,FinalNumber,FinalDn,BillCode,BillRate,BillCost,BillName,Chain,FromType,ToType,FinalType,FromDispname,ToDispname,FinalDispname) values (@RawLogId, @HistoryId,@CallId,@Duration,@TimeStart,@TimeAnswered,@TimeEnd,@ReasonTerminated,@FromNo,@ToNo,@FromDn,@ToDn,@DialNo,@ReasonChanged,@FinalNumber,@FinalDn,@BillCode,@BillRate,@BillCost,@BillName,@Chain,@FromType,@ToType,@FinalType,@FromDispname,@ToDispname,@FinalDispname);";
        
        // Where the log files are localed on the server. 
        // This should be the same for all servers, but I am not sure.
        public static string RemotePath = "/var/lib/3cxpbx/Instance1/Data/Logs/CDRLogs";

        /// <summary>
        /// Takes in the raw log string and returns a parsed list of SqlParameters
        /// </summary>
        /// <param name="rawLog"></param>
        /// <returns></returns>        
        public static List<SqlParameter> CreateSqlParamCollection(string rawLog)
        {
            // The raw log is a comma delimited string
            var values = rawLog.Split(',');
            return new List<SqlParameter>()
            {
                new SqlParameter(){ParameterName = "@HistoryId", Value = values[0]},
                new SqlParameter(){ParameterName = "@CallId", Value = values[1]},
                new SqlParameter(){ParameterName = "@Duration", Value = values[2]},
                new SqlParameter(){ParameterName = "@TimeStart", Value = values[3]},
                new SqlParameter(){ParameterName = "@TimeAnswered", Value = values[4]},
                new SqlParameter(){ParameterName = "@TimeEnd", Value = values[5]},
                new SqlParameter(){ParameterName = "@ReasonTerminated", Value = values[6]},
                new SqlParameter(){ParameterName = "@FromNo", Value = values[7]},
                new SqlParameter(){ParameterName = "@ToNo", Value = values[8]},
                new SqlParameter(){ParameterName = "@FromDn", Value = values[9]},
                new SqlParameter(){ParameterName = "@ToDn", Value = values[10]},
                new SqlParameter(){ParameterName = "@DialNo", Value = values[11]},
                new SqlParameter(){ParameterName = "@ReasonChanged", Value = values[12]},
                new SqlParameter(){ParameterName = "@FinalNumber", Value = values[13]},
                new SqlParameter(){ParameterName = "@FinalDn", Value = values[14]},
                new SqlParameter(){ParameterName = "@BillCode", Value = values[15]},
                new SqlParameter(){ParameterName = "@BillRate", Value = values[16]},
                new SqlParameter(){ParameterName = "@BillCost", Value = values[17]},
                new SqlParameter(){ParameterName = "@BillName", Value = values[18]},
                new SqlParameter(){ParameterName = "@Chain", Value = values[19]},
                new SqlParameter(){ParameterName = "@FromType", Value = values[20]},
                new SqlParameter(){ParameterName = "@ToType", Value = values[21]},
                new SqlParameter(){ParameterName = "@FinalType", Value = values[22]},
                new SqlParameter(){ParameterName = "@FromDispname", Value = values[23]},
                new SqlParameter(){ParameterName = "@ToDispname", Value = values[24]},
                new SqlParameter(){ParameterName = "@FinalDispname", Value = values[25]}
            };
        }
    }
}
