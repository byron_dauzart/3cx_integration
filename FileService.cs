using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using WinSCP;

namespace _3CX_Integration
{

    public static class FileService
    {
        // When you build this, the address to activate this function will be at the bottom of the window
        // Should be something like "http://localhost:7071/api/GetLogs"
        [FunctionName("GetLogs")]
        public static async Task<IActionResult> GetLogs([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req, ExecutionContext executionContext)
        {
            try
            {
                // TODO: Create list of servers to check that contains all of the data needed for session options and for the db name
                // This list needs to be able to be maintained and updated by Business Services (I think)
                // Not sure if that's a json or create a DB with all of the info (the ServerInfo object on the LogHelper has the fields that I have needed so far)

                // The session options are the server we're getting data from and the db name to know where to save that data
                var listOfServers = LogHelper.GetServerInfo(executionContext);
                foreach (ServerInfo info in listOfServers)
                {
                    var sessionOptions = new SessionOptions
                    {
                        Protocol = info.Protocol,
                        HostName = info.HostName,
                        UserName = info.UserName,
                        SshHostKeyFingerprint = info.SshHostKeyFingerPrint,
                        SshPrivateKeyPath = info.SshPrivateKeyPath
                    };
                    var dbName = info.DatabaseName;

                    using Session session = new Session
                    {
                        // The WinSCP.exe needs to be in the same file as the .dll or explicitly located.  
                        // Moving it into the same folder fixes the problem, but clean or rebuild clears the bin folder and you have to move it back in.
                        // The WinSCP.exe is now set to be copied with the "Files" folder so this should always get the WinSCP.exe
                        ExecutablePath = LogHelper.WinSCPFile(executionContext)
                    };
                    // Connect to session
                    session.Open(sessionOptions);

                    // Retrieve a list of files in a remote directory
                    RemoteDirectoryInfo directory = session.ListDirectory(LogHelper.RemotePath);


                    foreach (RemoteFileInfo fileInfo in directory.Files)
                    {
                        // Ignore any directories and any files that are not log files
                        if (!fileInfo.IsDirectory && fileInfo.Name.EndsWith(".log", StringComparison.OrdinalIgnoreCase))
                        {
                            // WinSCP doesn't read the contents of files it gets, so we temporarily save those files to a temp folder to read them.
                            // The temp files are deleted later.
                            string tempPath = Path.GetTempFileName();

                            // Download the file to a temporary folder
                            var sourcePath = RemotePath.EscapeFileMask(LogHelper.RemotePath + "/" + fileInfo.Name);
                            // The third param for GetFiles is to remove after they are retrieved so when this actually starts being used, we will probably turn that to true
                            session.GetFiles(sourcePath, tempPath, false).Check();

                            // Read the contents of the temp file. 
                            // The log file currently has one line we care about and a second empty line, so we may want to 
                            // validate the contents of the lines or figure out how to get rid of the second line
                            string[] lines = File.ReadAllLines(tempPath);


                            using (SqlConnection connection = new SqlConnection(LogHelper.ConnectionString(dbName)))
                            {
                                connection.Open();

                                // Build the sql command to save the raw log
                                var rawlogCommand = new SqlCommand(LogHelper.RawQuery, connection);
                                rawlogCommand.Parameters.AddWithValue("@Name", fileInfo.Name);
                                rawlogCommand.Parameters.AddWithValue("@String", lines[0]);

                                // Save the file name and raw log to the db.  The query string has an "output RawLogId" clause.
                                // RawLogId is the id of the new row
                                var RawLogId = (int)rawlogCommand.ExecuteScalar();

                                // If RawLogId<=0, then no rows were affected (the entry failed)
                                if (RawLogId <= 0)
                                {
                                    // Log the "bad" file somewhere so it can be traced
                                    // Files aren't currently being deleted during the "GetFiles()" but when they are being deleted in the future, we need to actually save that file somewhere
                                }
                                else
                                {
                                    // Build the sql command to save the parsed log.  Adding the RawLogId to build a connection
                                    var parsedLogCommand = new SqlCommand(LogHelper.ParseQuery, connection);
                                    parsedLogCommand.Parameters.AddRange(LogHelper.CreateSqlParamCollection(lines[0]).ToArray());
                                    parsedLogCommand.Parameters.AddWithValue("@RawLogId", (int)RawLogId);
                                    var res = parsedLogCommand.ExecuteNonQuery();
                                }
                            }

                            // Delete the temporary copy
                            File.Delete(tempPath);
                        }
                    }
                };
                return new OkObjectResult("Success");
            }
            catch (Exception e)
            {
                return new OkObjectResult("Fail: " + e.Message);
            }
        }

    }
}
